#Readme

###SQL Exporte
Im Ordner .auszug_sql befinden sich Dateien (Exporte) unserer MySQL. Die HTML Dateien sind nicht über localhost erreichbar,
sie müssen per Hand im Browser geöffnet werden.
##
###Background Crawler
Er ist durch die Datei CrawlerRunCommand.php realisiert, darin wird ein Befehl definiert, welcher auf dem Server über
Symfony ausgeführt werden kann.
###
Dadurch, dass ein Background Crawler genutzt wird, wird beim hinzufügen eines Links, wie im IndexController beschrieben,
nur diese Seite, und nicht noch alle verlinkten Seiten, gecrawled. Die verlinkten Seiten werden irgendwann vom Background 
Crawler erfasst und gecrawled.
##
###Bilder
Diese finden Sie im Ordner .ui
###
Bild1 zeigt beispielhaft die Suche nach dem Wort "dhbw" werden logischerweise alle 77 gecrawlten Seiten der DHBW gefunden.
###
Bild2 zeigt das Aussehen, wenn keine Treffer gefunden werden.
###
Bild3 ist eine kleine Demonstration der Funktionsweise des Crawlers. Wie man sieht taucht das Wort "rolf" auf der Seite
nicht auf, jedenfalls nicht direkt (Suche über Strg + F), da es aktuell "unsichtbar ist", im HTML der Seite, wie im Screenshot
gezeigt, taucht das Wort jedoch auf. Dementsprechend zeigt Bild4 das richtige Ergebnis an, dass das Wort "rolf" auf der
Seite gefunden wird, obwohl es nicht direkt sichtbar ist. Das Wort "rolf" befindet sich in einem Tab, welches aktuell
ausgeblendet war.

##
###Eingebaute zusätzlichen Features
* Stopwortfilter -> Service/WordService
* Background Crawler -> Command/CrawlerRunCommand
* Seitentitel der Seite wird bei den Ergebnissen angezeigt
* Timestamps des letzten Besuchs werden aufgezeichnet und nur unbesuchte und Links älter als ein Tag werden vom Background
Crawler gecrawled
* Anzahl der gefunden Links bei der Wortsuche werden angezeigt
* Deutsche Sonderzeichen werden ersetzt, auch in der Suche (bspw. ä wird zu ae)