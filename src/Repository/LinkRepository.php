<?php

namespace App\Repository;

use App\Entity\Link;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Link|null find($id, $lockMode = null, $lockVersion = null)
 * @method Link|null findOneBy(array $criteria, array $orderBy = null)
 * @method Link[]    findAll()
 * @method Link[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    /**
     * @param $url
     * @return Link|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByUrl($url): ?Link
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.url = :url')
            ->setParameter('url', $url)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getUnvisitedLinks()
    {
        $unvisited = $this->findBy(['timestamp' => null]);
        $old = $this->createQueryBuilder('l')
            ->orWhere('l.timestamp < :timestamp')
            ->setParameter('timestamp',new \DateTime('now -1 day'))
            ->getQuery()
            ->getResult();

        /**
         * unbesuchte Links sind zu crawler, dazu zählen alle die, die noch nie beuscht wurden (timestamp === null)
         * und alle die, wo der timestamp älter ist als einen Tag (now - 1 day)
         */
        return array_merge($unvisited, $old);
    }

    public function getNextUnvisitedLink()
    {
        $unvisited = $this->findBy(['timestamp' => null]);
        $old = $this->createQueryBuilder('l')
            ->orWhere('l.timestamp < :timestamp')
            ->setParameter('timestamp',new \DateTime('now -1 day'))
            ->getQuery()
            ->getResult();

        $mergedArray = array_merge($unvisited, $old);

        if (count($mergedArray) === 0)
        {
            return null;
        }
        elseif (count($mergedArray) > 0)
        {
            return $mergedArray[0];
        }
    }
}
