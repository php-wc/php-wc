<?php

namespace App\Repository;

use App\Entity\Word;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Word|null find($id, $lockMode = null, $lockVersion = null)
 * @method Word|null findOneBy(array $criteria, array $orderBy = null)
 * @method Word[]    findAll()
 * @method Word[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Word::class);
    }

    public function findByWord($word): ?Word
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.word = :word')
            ->setParameter('word', $word)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findByWordLike($word)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.word like :word')
            ->setParameter('word', '%'.$word.'%')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findWords()
    {
        $words = $this->createQueryBuilder('w')
            ->select(
                'w.word'
            )
            ->getQuery()
            ->getResult();

        if ($words)
        {
            return array_column($words, 'word');
        }
        else
        {
            return [];
        }
    }

    // /**
}
