<?php

namespace App\Service;

use App\Entity\Link;
use App\Entity\Word;
use App\Repository\LinkRepository;
use App\Repository\WordRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

class Webcrawler
{
    /** @var HttpClient */
    private $httpClient;
    /** @var LinkRepository */
    private $linkRepository;
    /** @var WordRepository */
    private $wordRepository;
    /** @var EntityManagerInterface */
    private $em;
    /** @var WordService */
    private $wordService;

    public function __construct(EntityManagerInterface $em, WordService $wordService)
    {
        $this->httpClient = HttpClient::create();
        $this->em = $em;
        $this->linkRepository = $this->em->getRepository('App\Entity\Link');
        $this->wordRepository = $this->em->getRepository('App\Entity\Word');
        $this->wordService = $wordService;
    }

    private function requestDataFromUrl($url)
    {
        $response = $this->httpClient->request('GET', $url);
        $statuscode = $response->getStatusCode();
        if ($statuscode !== 200)
        {
            throw new \Exception("Statuscode: ".$statuscode);
        }
        $content = $response->getContent();
        $info = $response->getInfo();
        $headers = $response->getHeaders();

        //TODO: Hätte eigentlich ein selbstgeschriebenes Request Objekt sein müssen
        return array(
            'info' => $info,
            'headers' => $headers,
            'content' => $content
        );
    }

    private function getBaseUrl($responseArray)
    {
        //Besorgt den Host und das Scheme um Links später in ein einheitliches Format bringen zu können
        $urlArray = parse_url($responseArray['info']['url']);
        return $urlArray['scheme'].'://'.$urlArray['host'];
    }

    private function getCrawlerForContent($content)
    {
        $crawler = new Crawler($content);

        return $crawler;
    }

    private function getContent($responseArray)
    {
        $crawler = $this->getCrawlerForContent($responseArray['content']);
        $html = $crawler->filter('body');
        $html = $html->html();
        //entferne script tags
        $clear = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
        /**
         * bevor alle anderen Tags entfernt werden, muss ein Leerzeichen eingefügt werden, da sonst aus
         * <h1>Hallo</h1><h3>Welt</h3>
         * HalloWelt
         * wird, deshalb muss nach schließenden Tag Klammern ein Leerzeichen eingefügt werden, das später wieder
         * entfernt wird
         */
        $clear = str_replace('>','> ',$clear);
        //entferne alle Tags
        $clear = preg_replace('/<[^>]+?>/', ' ', $clear);
        //$clear = strip_tags($clear); funktioniert nicht!!! text geht verloren!!!
        $clear = html_entity_decode($clear);
        $clear = urldecode($clear);
        $clear = strtolower($clear);
        $clear = str_replace('ä','ae',$clear);
        $clear = str_replace('ö','oe',$clear);
        $clear = str_replace('ü','ue',$clear);
        $clear = str_replace('ß','ss',$clear);
        $clear = preg_replace('/[^A-Za-z0-9]/', ' ', $clear);
        $clear = preg_replace('/ +/', ' ', $clear);
        $clear = trim($clear);
        $wordArray = preg_split("/\s+/", $clear);
        return array_unique($wordArray);
    }

    private function getTitle($responseArray)
    {
        //returnt den Seitentitel
        $crawler = $this->getCrawlerForContent($responseArray['content']);
        return $crawler = $crawler->filter('title')->text();
    }

    private function getAllLinks($responseArray)
    {
        //returnt einen Array mit allen Links auf einer Seite
        $parsedUrl = parse_url($this->getBaseUrl($responseArray));
        $host = $parsedUrl['host'];
        $scheme = $parsedUrl['scheme'];
        $crawler = $this->getCrawlerForContent($responseArray['content']);
        $crawler = $crawler->filter('a');
        $links = [];
        foreach ($crawler as $domElement)
        {
            /** @var \DOMElement $domElement */
            $link = $domElement->attributes->getNamedItem('href')->nodeValue;
            $link = parse_url($link);
            $link = $this->buildLink($scheme,$host,$link);
            $link!==null?array_push($links,$link):null;
        }
        $links = array_unique($links);
        return $links;
    }

    public function crawl($url)
    {
        try
        {
            $responseArray = $this->requestDataFromUrl($url);
            $link = $this->findLink($url);

            //Link existiert noch nicht
            if ($link === null)
            {
                /** @var Link $link */
                $link = $this->createNewLink($url);
                $link = $this->updateLink($link, $this->getTitle($responseArray));
                $this->updateWords($link, $responseArray);
                $links = $this->getAllLinks($responseArray);
                foreach ($links as $link)
                {
                    //$this->crawl($link); recursive crawl - ist deaktiviert, darum kümmert sich der Crawler im Background
                    $linkObject = $this->findLink($link);
                    if ($linkObject === null)
                    {
                        $linkObject = $this->createNewLink($link);
                    }
                }
            }
            //Link ist älter als 1 Tag
            elseif ($link->getTimestamp() > new \DateTime('now +1 day') || $link->getTimestamp() === null)
            {
                $this->updateLink($link, $this->getTitle($responseArray));
                $this->updateWords($link, $responseArray);
                $links = $this->getAllLinks($responseArray);
                foreach ($links as $link)
                {
                    //$this->crawl($link); recursive crawl
                    $linkObject = $this->findLink($link);
                    if ($linkObject === null)
                    {
                        $linkObject = $this->createNewLink($link);
                    }
                }

            }
            //Link muss nicht durchsucht werden
            else
            {
                //nothing to create or update
            }
        }
        catch (\Exception $e)
        {
            return 0;
        }

        return 1;
    }

    /**
     * Baut Links in ein einheitliches Format
     * Anker (#) werden dabei ignoiert
     */
    private function buildLink($scheme, $host, $linkArray)
    {
        $path = '';
        $query = '';
        if (key_exists('host', $linkArray))
        {
            if ($linkArray['host'] !== $host)
            {
                return null;
            }
        }
        if (key_exists('path', $linkArray))
        {
            $path = $linkArray['path'];
            if (substr( $path, 0, 1 ) !== "/")
            {
                return null;
            }
        }
        if (key_exists('query', $linkArray))
        {
            $query = '?'.$linkArray['query'];
        }
        return $scheme.'://'.$host.$path.$query;
    }

    /*******************************************************************************************************************
     *
     * funktionen, die mit den repositories kommunizieren um daten zu persistieren und zu beziehen
     *
     ******************************************************************************************************************/

    private function findLink($url)
    {
        $linkObject = $this->linkRepository->findByUrl($url);
        return $linkObject;
    }

    private function createNewLink($url)
    {
        $linkObject = new Link($url);
        $this->em->persist($linkObject);
        $this->em->flush();

        return $linkObject;
    }

    private function updateLink(Link $link, $title)
    {
        $link->setTimestamp(new \DateTime('now'));
        $link->setTitel($title);
        $this->em->flush();

        return $link;
    }

    private function updateWords(Link $link, $responseArray)
    {
        $link->removeWords();
        $wordsFromUrl = $this->getContent($responseArray);
        //hier werden die stopworte herausgefiltert
        $wordsFromUrl = $this->wordService->filterStopWords($wordsFromUrl);
        foreach ($wordsFromUrl as $word)
        {
            $wordObject = $this->wordRepository->findByWord($word);
            //Wort existiert noch nicht in der DB
            if ($wordObject === null){
                $wordObject = new Word($word);
                $this->em->persist($wordObject);
            }
            //Wort-Link Verknüpfung wird erstellt
            $link->addWord($wordObject);
        }

        $this->em->flush();

        return 1;
    }
}