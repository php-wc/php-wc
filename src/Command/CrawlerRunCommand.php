<?php

namespace App\Command;

use App\Entity\Link;
use App\Repository\LinkRepository;
use App\Service\Webcrawler;
use App\Service\WordService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CrawlerRunCommand extends Command
{
    protected static $defaultName = 'app:crawler:run';

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var WordService
     */
    private $wordService;
    /** @var LinkRepository */
    private $linkRepository;

    protected function configure()
    {
        $this
            ->setDescription('This command starts the webcrawler')
        ;
    }

    public function __construct(EntityManagerInterface $em, WordService $wordService)
    {
        $this->em = $em;
        $this->wordService = $wordService;
        $this->linkRepository = $this->em->getRepository('App\Entity\Link');

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $output->writeln('Starte Crawler...');
        sleep(2);
        $crawler = new Webcrawler($this->em, $this->wordService);
        $io->success('Crawler gestartet');
        sleep(2);

        while (true) {
            $link = $this->linkRepository->getNextUnvisitedLink();
            if ($link)
            {
                $output->writeln("Crawling: ".$link->getUrl());
                $crawler->crawl($link->getUrl());

            }
            $this->em->clear();
        }

        return 0;
    }

    /**
     *
     * alte Version des Background Crawlers, welche nicht permanent im Hintergrund läuft, sondern ein Set an
     * unbesuchten Links abarbeitet und sich dann beendet
     * Alle unbesuchten und alten Links, die zum Zeitpunkt der Ausführung dieses Befehls bestehen, werden gecrawled
     *
     *
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $output->writeln('Starte Crawler...');
        sleep(2);
        $crawler = new Webcrawler($this->em, $this->wordService);
        $io->success('Crawler gestartet');
        sleep(2);
        $links = $this->linkRepository->getUnvisitedLinks();

        foreach ($links as $link)
        {
            $output->writeln("Crawling: ".$link->getUrl());
            $crawler->crawl($link->getUrl());

        }

        $io->success('Crawlen erfolgreich!');

        return 0;
    }
    **/
}
