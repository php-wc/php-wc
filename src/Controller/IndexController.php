<?php


namespace App\Controller;
use App\Form\AddNewLinkType;
use App\Form\SearchWordType;
use App\Repository\WordRepository;
use App\Service\Webcrawler;
use App\Service\WordService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(Request $request,Webcrawler $webcrawler, WordRepository $wordRepository, WordService $wordService)
    {
        $formAdd = $this->createForm(AddNewLinkType::class);
        $formSearch = $this->createForm(SearchWordType::class);

        /**
         * Add Link to DB
         */
        $formAdd->handleRequest($request);
        if ($formAdd->isSubmitted() && $formAdd->isValid()) {
            //Der Link wird in die DB geschrieben, gecrawlt und alle Wörter und Links darin werden in die DB übertragen
            //Die gefundenen Links werden hier nicht gecrawlt, das übernimmt der Crawler, welcher im Hintergrund läuft.
            $link = $formAdd->getData()['link'];
            $webcrawler->crawl('http://'.$link);

        }

        /**
         * Search Links by Word
         */
        $formSearch->handleRequest($request);
        $links = null;
        if ($formSearch->isSubmitted() && $formSearch->isValid()) {
            $links = [];
            $word = ($formSearch->getData())['word'];
            $word = $wordService->clearWordFromSpecialCharacters($word);
            $word = $wordRepository->findByWord($word);
            if($word)
            {
                $links = $word->getLinks()->toArray();
            }
        }

        /**
         * $links == null -> kein Submit stattgefunden
         * $links == [] -> Submit, aber keine Ergebnisse gefunden
         * $links == [...] -> Submit, und Ergebnisse gefunden
         * -> Für entsprechende Anzeige im Frontend wichtig
         */

        return $this->render('base.html.twig', [
        'formAdd' => $formAdd->createView(),
        'formSearch' => $formSearch->createView(),
        'links' => $links
    ]);

    }
}